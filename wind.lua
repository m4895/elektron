
local add_power = elektron.add_power
local rotate_align = elektron.geometry.rotate_align

local windspeed = 0
local last_changed = 0

local def = {
	description = "Windmill",
	paramtype = 'light',
	paramtype2 = "4dir",
	tiles = { 
		"elektron_white.png", "elektron_switch_out.png",
		"elektron_white.png", "elektron_white.png",
		{
				name = "elektron_windmill_back.png",
				animation = {type = "vertical_frames", aspect_w = 32, aspect_h = 32, length = 0.5},
		},
		{
				name = "elektron_windmill.png",
				animation = {type = "vertical_frames", aspect_w = 32, aspect_h = 32, length = 0.5},
		},
	false},
	use_texture_alpha = "false",
	sunlight_propagates = true,
	groups = {oddly_breakable_by_hand = 3},

	elektron_tick = function(pos, device)
		local output = device.wires and device.wires.bottom
		if not output then return end
		
		-- change windspeed (10 times day at even intervals)
		local current = math.floor(minetest.get_timeofday()*10)
		if current ~= last_changed then
			windspeed = math.random(4) - 1
			last_changed = current
		end
		if windspeed == 0 then return end

		local node = minetest.get_node(pos)

		local back_dir = minetest.fourdir_to_dir(node.param2)

		local backpos1 = pos:add(back_dir)
		local backpos2 = pos:add(back_dir:multiply(6))
		local frontpos1 = pos:add(back_dir:multiply(-1))
		local frontpos2 = pos:add(back_dir:multiply(-6))

		local is_back_clear = minetest.line_of_sight(backpos1, backpos2)
		if not is_back_clear then return end

		local is_front_clear = minetest.line_of_sight(frontpos1, frontpos2)
		if not is_front_clear then return end

		add_power(output, windspeed)
	end,
}

elektron.register_device("elektron:windmill", def)

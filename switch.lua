local add_power = elektron.add_power
local register_device = elektron.register_device
local read_signal = elektron.read_signal
local get_side = elektron.geometry.get_side_pos
local get_sides_in_group = elektron.geometry.get_sides_in_group
local v = elektron.geometry

local def = {
	description = 'Elektron Switch',
	tiles = {
		'elektron_switch_out.png', 'elektron_gate_power.png', -- top, bottom
		'elektron_gate_in.png', 'elektron_gate_in.png', -- right, left
		'elektron_gate_in.png', 'elektron_gate_in.png', -- back, front
	},

	groups = {oddly_breakable_by_hand=3 },
	paramtype = 'light',

	elektron_tick = function(pos, device)
		local wire = device.wires
		if not wire.top then return end
		if not wire.bottom then return end

		local signal = wire.left and read_signal(wire.left) 
			or wire.right and read_signal(wire.right)
			or wire.front and read_signal(wire.front)
			or wire.back  and read_signal(wire.back)

		if signal then
			elektron.add_bridge(wire.bottom, wire.top)
		end
	end,

}
register_device("elektron:switch",def)

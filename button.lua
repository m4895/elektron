
local def = {
	description = 'Elektron Button',
	tiles = { 'elektron_button_off.png' },
	groups = {oddly_breakable_by_hand=3 },
	paramtype = 'light',

	on_rightclick = function(pos, node, puncher, pointed_thing)
		minetest.swap_node(pos, {name="elektron:button_on"})
	end,

	on_punch = function(pos, node, puncher, pointed_thing)
		minetest.swap_node(pos, {name="elektron:button_on"})
		minetest.after(1, function() 
			local node = minetest.get_node(pos)
			if node.name == "elektron:button_on" then
				minetest.swap_node(pos, {name="elektron:button"})
			end
		end)
	end,
}
elektron.register_device("elektron:button", def)

local def_on = {
	description = 'Elektron Button',
	tiles = {'elektron_button_on.png' },
	groups = {not_in_creative_inventory=1, oddly_breakable_by_hand=3 },
	paramtype = 'light',

	elektron_tick = function(pos, device)
		for _, wire in pairs(device.wires) do
			elektron.send_signal(wire)
		end
	end,

	on_rightclick = function(pos, node, puncher, pointed_thing)
		minetest.swap_node(pos, {name="elektron:button"})
	end,

	drop = "elektron:button",
}
elektron.register_device("elektron:button_on",def_on)


elektron = {}

local mod_path = minetest.get_modpath("elektron")

-- helper functions
dofile(mod_path.."/geometry.lua")
dofile(mod_path.."/fake_player.lua")

-- main functionality
dofile(mod_path.."/wire.lua")

-- devices
dofile(mod_path.."/lamp.lua")
dofile(mod_path.."/gate.lua")
dofile(mod_path.."/button.lua")
dofile(mod_path.."/wind.lua")
dofile(mod_path.."/solar.lua")
dofile(mod_path.."/activator.lua")
dofile(mod_path.."/switch.lua")
dofile(mod_path.."/battery.lua")
dofile(mod_path.."/placer.lua")

-- recipes
dofile(mod_path.."/recipes.lua")


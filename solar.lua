local add_power = elektron.add_power
local register_device = elektron.register_device
local find_wire = elektron.find_wire

local def = {
	description = 'Elektron Solar Power Generator',
	tiles = {
		'elektron_solar.png', 'elektron_switch_out.png', 
		'elektron_solar.png', 'elektron_solar.png', 
		'elektron_solar.png', 'elektron_solar.png', 
		},
	paramtype = 'light',
	groups = {oddly_breakable_by_hand = 3},

	elektron_tick = function(pos, device)
		
		local above = pos + vector.new(0, 1, 0)

		local node = minetest.get_node(above)
		if node.name ~= "air" then return end 

		local light = minetest.get_node_light(above)
		if light > 11 then
			local _, output = next(device.wires)
			light = light - 11 
			add_power(output, light)
		end
	end,
}

register_device("elektron:solar", def)

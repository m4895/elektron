local add_power = elektron.add_power
local read_signal = elektron.read_signal
local send_signal = elektron.send_signal
local register_device = elektron.register_device
local rotate_and_place = elektron.geometry.rotate_and_place
local get_side = elektron.geometry.get_side_pos
local v = elektron.geometry

local def = {
	description = 'Elektron Logic Gate',
	tiles = {
		'elektron_gate_not.png',  'elektron_gate_power.png', -- top, bottom
		'elektron_gate_in.png','elektron_gate_xor.png', -- right, left
		'elektron_gate_and.png', 'elektron_gate_in.png', -- back, front
	},
	groups = {elektron_device = 1, oddly_breakable_by_hand=3 },
	paramtype = 'light',
	paramtype2 = 'facedir',

	elektron_tick = function(pos, device)

		local wire = device.wires

		local power = add_power(wire.bottom, -0.1)
		if not power then return end

		local sig_not = wire.top and read_signal(wire.top)
		local sig_a = wire.front and read_signal(wire.front)
		local sig_b = wire.right and read_signal(wire.right)
		
		local and_result = sig_a and sig_b
		local xor_result = (sig_a or sig_b) and not and_result

		if sig_not then
			and_result = not and_result
			xor_result = not xor_result
		end

		if and_result and power and wire.back then
			send_signal(wire.back)
		end
		

		if xor_result and power and wire.left then
			send_signal(wire.left)
		end

	end,

	on_place = function(itemstack, placer, pointed_thing)
		rotate_and_place(itemstack, placer, pointed_thing)
	end,
}

register_device("elektron:gate",def)

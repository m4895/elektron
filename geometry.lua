
elektron.geometry = {}
local g = elektron.geometry
local find_in_box = minetest.find_nodes_in_area

g.top = vector.new(0, 1, 0)
g.bottom = vector.new(0, -1, 0)
g.back = vector.new(0, 0, 1)
g.front = vector.new(0, 0, -1)
g.right = vector.new(1, 0, 0)
g.left = vector.new(-1, 0, 0)

g.one = vector.new(1, 1, 1)

g.get_side_pos = function(side, pos)
	local node = minetest.get_node(pos)
	return pos + g.rotate_align(side, node.param2)
end

g.rotate_and_place = function(itemstack, placer, pointed_thing)
	local p0 = pointed_thing.under
	local p1 = pointed_thing.above
	local param2 = 0

	if placer then
		local placer_pos = placer:get_pos()
		if placer_pos then
			param2 = minetest.dir_to_facedir(vector.subtract(p1, placer_pos), true)
		end
	end
	return minetest.item_place(itemstack, placer, pointed_thing, param2)
end

g.rotate_align = function (v, param2) 

	local r1 = param2 % 4
	local r2 = math.floor(param2 / 4)
	local d90 = math.pi/2

	v = vector.rotate_around_axis(v, g.bottom, d90*r1 )

	if r2 == 1 then 
		return vector.rotate_around_axis(v, g.right, -d90)
	elseif r2 == 2 then
		return vector.rotate_around_axis(v, g.right, d90)
	elseif r2 == 3 then 
		return vector.rotate_around_axis(v, g.front, -d90)
	elseif r2 == 4 then
		return vector.rotate_around_axis(v, g.front, d90)
	elseif r2 == 5 then 
		return vector.rotate_around_axis(v, g.front, d90*2)
	else
		return v
	end
end

local directions = {
	left = vector.new(-1, 0, 0),
	right = vector.new(1, 0, 0),
	back = vector.new(0, 0, 1),
	front = vector.new(0, 0, -1),
	top = vector.new(0, 1, 0),
	bottom = vector.new(0, -1, 0),
}

g.get_sides_in_group = function(pos, groupname)
	local node = minetest.get_node(pos)
	local def = minetest.registered_nodes[node.name]

	local found = {}
	for dir_name, dir_pos in pairs(directions) do
		local side_pos

		if def.paramtype2 == "facedir" and node.param2 then
			side_pos = pos + g.rotate_align(dir_pos, node.param2)
		else
			side_pos = pos + dir_pos
		end
		local side_node = minetest.get_node(side_pos)
		local grp = minetest.get_item_group(side_node.name, groupname)
		if grp and grp > 0 then
			found[dir_name] = side_pos
		end
	end
	if not next(found) then return end
	return found
end

g.find_facing = function(pos, node_name)
	local facing = {}

	local in_box = find_in_box(pos - g.one, pos + g.one, node_name)

	for _, p in pairs(in_box) do
		local found_dir = 0
		for _, v in pairs(pos - p) do
			if v ~= 0 then
				found_dir = found_dir + 1
			end
		end

		if found_dir == 1 then
			table.insert(facing, p)
		end
			
	end

	return facing
end

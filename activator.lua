local add_power = elektron.add_power
local rotate_align = elektron.geometry.rotate_align

local function do_rightclick(target, owner)
	local target_node = minetest.get_node_or_nil(target)

	if target_node then
		local target_def = minetest.registered_nodes[target_node.name]
		if target_def and target_def.on_rightclick then
			local robot = elektron.create_fake_player({name = owner})
			target_def.on_rightclick(target, target_node, robot) 
			return true
		end
	end
	return
end

local def = {
	description = 'Elektron Activator',
	tiles = {
		'elektron_activator.png', 'elektron_gate_power.png', -- top, bottom
		'elektron_gate_power.png','elektron_gate_power.png', -- right, left
		'elektron_gate_power.png','elektron_gate_power.png', -- back, front
	},
	groups = {oddly_breakable_by_hand = 3},
	paramtype = 'light',
	paramtype2 = 'facedir',

	after_place_node = function(pos, placer) 
		local meta = minetest.get_meta(pos)
		local owner = placer:get_player_name() or ""
		meta:set_string("owner", owner)
	end,

	elektron_tick = function(pos, device)
		local _, in_power = next(device.wires) -- just pick first connected wire
		local meta = minetest.get_meta(pos)
		local power = add_power(in_power, -1)

		if power and not device.is_on then
			local node = minetest.get_node(pos)
			local target_offset = rotate_align(vector.new(0, 1, 0), node.param2)
			local target = pos + target_offset
			local owner = meta:get_string("owner")
			local done = do_rightclick(target, owner)

			if not done then 
				target = pos + (2 * target_offset)
				do_rightclick(target, owner)
			end
		end

		if power then
			device.is_on = true
		else
			device.is_on = nil
		end
	end,
}

elektron.register_device("elektron:activator", def)

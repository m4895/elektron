
local node = minetest.registered_nodes

local has_mod = function (name) 
	return minetest.get_modpath(name) ~= nil
end

-- source abstract ingredients from what is available in mods
local SPECIAL, GLASS, COPPER, CIRC1, CIRC2, METAL, SOLDR, ANODE, CTHOD,
	REDDY, BLUDY, GRNDY

if (has_mod("default")) then
	GLASS="default:glass"
	CIRC1="default:mese_crystal_fragment"
	CIRC2="default:mese_crystal"
	METAL="default:copper_ingot"
	SOLDR="default:tin_ingot"
	ANODE="default:bronze_ignot"
	CTHOD="default:steel_ignot"
end

if (has_mod("zr_glass")) then
	GLASS="glass:glass"
end

if (has_mod("zr_mese")) then
	CIRC1="zr_mese:mese_crystal_fragment"
	CIRC2="zr_mese:mese_crystal"
end

if (has_mod("zr_mese")) then
	CIRC1="zr_mese:crystal_fragment"
	CIRC2="zr_mese:crystal"
end

if (has_mod("zr_glass"))  then GLASS="zr_glass:glass" end
if (has_mod("zr_tin"))    then SOLDR="zr_tin:ingot" end
if (has_mod("zr_copper")) then METAL="zr_copper:ingot" end
if (has_mod("zr_bronze")) then ANODE="zr_bronze:ingot" end
if (has_mod("zr_iron"))   then CTHOD="zr_iron:ingot" end

if (has_mod("dye")) then
	REDDY = "dye:red"
	BLUDY = "dye:blue"
	GRNDY = "dye:green"
end

if (has_mod("zr_dye")) then
	REDDY = "zr_dye:red"
	BLUDY = "zr_dye:blue"
	GRNDY = "zr_dye:green"
end

--- RECIPES
if node["elektron:activator"] and METAL and SOLDR and CIRC2 then
	minetest.register_craft({
		output = "elektron:activator",
		recipe = {
			{METAL, CIRC2, METAL},
			{METAL, METAL, METAL},
			{METAL, SOLDR, METAL},
		}
	})
end
if node["elektron:switch"] and METAL and SOLDR and CIRC1 then
	minetest.register_craft({
		output = "elektron:switch",
		recipe = {
			{SOLDR, METAL, SOLDR},
			{METAL, CIRC1, METAL},
			{SOLDR, METAL, SOLDR},
		}
	})
end
if node["elektron:battery"] and METAL and ANODE and CTHOD then
	minetest.register_craft({
		output = "elektron:battery",
		recipe = {
			{ANODE, "",    CTHOD},
			{METAL, METAL, METAL},
			{METAL, METAL, METAL},
		}
	})
end
if node["elektron:gate"] and METAL and CIRC1 and SOLDR then
	minetest.register_craft({
		output = "elektron:gate",
		recipe = {
			{SOLDR, SOLDR, SOLDR},
			{CIRC1, CIRC1, CIRC1},
			{METAL, METAL, METAL},
		}
	})
end
if node["elektron:lamp"] and METAL and CIRC1 and SOLDR then
	minetest.register_craft({
		output = "elektron:lamp 4",
		recipe = {
			{GLASS, GLASS, GLASS},
			{GLASS, METAL, GLASS},
			{GLASS, GLASS, GLASS},
		}
	})
end
if node["elektron:solar"] and METAL and GLASS and CIRC2 then
	minetest.register_craft({
		output = "elektron:solar",
		recipe = {
			{GLASS, GLASS, GLASS},
			{METAL, METAL, METAL},
			{CIRC2, CIRC2, CIRC2},
		}
	})
end
if node["elektron:button"] and METAL and ANODE and SOLDR then
	minetest.register_craft({
		output = "elektron:button",
		recipe = {
			{"",    CIRC1, ""   },
			{"",    SOLDR, ""   },
			{METAL, METAL, METAL},
		}
	})
end
if node["elektron:red_wire"] and METAL and REDDY then
	minetest.register_craft({
		output = "elektron:red_wire 8",
		recipe = {
			{"",    REDDY, ""   },
			{METAL, METAL, METAL},
		}
	})
end
if node["elektron:blue_wire"] and METAL and BLUDY then
	minetest.register_craft({
		output = "elektron:blue_wire 8",
		recipe = {
			{"",    BLUDY,  ""   },
			{METAL, METAL,  METAL},
		}
	})
end
if node["elektron:green_wire"] and METAL and GRNDY then
	minetest.register_craft({
		output = "elektron:green_wire 8",
		recipe = {
			{"",    GRNDY, ""   },
			{METAL, METAL, METAL},
		}
	})
end



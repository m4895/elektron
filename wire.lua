-- ring_map is only for wires, not devices
local ring_map= {} -- pos (of wire) -> ring
local ring_info={} -- ring_id -> info
local ring_next_id = 1;
local devices = {} -- set of positions of active devices
local bridge = {} -- set of positions of active devices
local bridge_next = {} -- set of positions of active devices
local find_facing = elektron.geometry.find_facing
local get_sides = elektron.geometry.get_sides_in_group
local DEFAULT_RING_INFO = {power = 0, capacity = 20}

elektron.read_signal = function(pos)
	local ring_id = ring_map[vector.to_string(pos)]
	if not ring_id then return end

	local ring = ring_info[ring_id]
	if not ring then return end

	return ring.signal
end

elektron.send_signal = function(pos)
	local ring_id = ring_map[vector.to_string(pos)]
	local ring = ring_info[ring_id]
	if ring then
		ring.next_signal = true
	end
end

elektron.read_power = function (pos) 
	local ring_id = ring_map[vector.to_string(pos)]
	if not ring_id then return nil end

	local power = ring_info[ring_id].power

	if power <= 0 then return end
	return power
end

elektron.add_power = function (wire_pos, power) 
	if not wire_pos then return end

	local ring_id = ring_map[vector.to_string(wire_pos)]
	if not ring_id then return end
	local ring = ring_info[ring_id]

	if ring.power + power < 0 and power < 0 then return end

	ring.power = ring.power + power
	return power
end

elektron.add_capacity = function (wire_pos, capacity) 
	if not wire_pos then return end

	local ring_id = ring_map[vector.to_string(wire_pos)]
	if not ring_id then return end
	local ring = ring_info[ring_id]

	ring.capacity = ring.capacity + capacity
	return capacity
end

elektron.find_wire = function (pos)
	local wires = find_facing(pos, "group:elektron_wire")
	local _, wire = next(wires)
	return wire
end

elektron.find_wires = function (pos)
	local wires = find_facing(pos, "group:elektron_wire")
	return wires
end

local add_device = function(pos)
	local pos_idx = vector.to_string(pos)
	local connections = get_sides(pos, "elektron_wire")
	if connections then
		devices[pos_idx] = { wires = connections }

		-- call _connect callback if it exists
		local node = minetest.get_node_or_nil(pos)
		if node then
			local def = minetest.registered_nodes[node.name]
			if def.elektron_connect then
				def.elektron_connect(pos, devices[pos_idx])
			end
		end
	end
end

local add_wire = function(pos, node) 
	local wire_name = node.name
	local pos_idx = vector.to_string(pos)
	if ring_map[pos_idx] then return end

	local join_ring_idx
	local merge = {}
	local domerge = false
	
	local connections = find_facing(pos, wire_name)
	-- find nodes in 3x3 box around position

	for _, p in pairs(connections) do
		local ring_idx = ring_map[vector.to_string(p)]
		if ring_idx then
			if not join_ring_idx then
				join_ring_idx = ring_idx
			elseif join_ring_idx ~= ring_idx then 
				merge[ring_idx] = true
			end
		end
	end

	-- if unconnected, create new ring
	if not join_ring_idx then
		ring_info[ring_next_id] = table.copy(DEFAULT_RING_INFO)
		join_ring_idx = ring_next_id 
		ring_next_id = ring_next_id +1
	end

	ring_map[pos_idx] = join_ring_idx

	if next(merge) then
		for p, ring_idx in pairs(ring_map) do
			if merge[ring_idx] then
				ring_map[p] = join_ring_idx
			end
		end

		for old, _ in pairs(merge) do
			ring_info[old] = nil
		end
	end

	-- fix any devices
	local connected_devices = get_sides(pos, "elektron_device")
	if not connected_devices then return end
	for _, dev_pos in pairs(connected_devices) do
		add_device(dev_pos)
	end
	return
end

local remove_wire = function(pos, node) 
	local wire_name = node.name
	local wires_to_fix = {}
	local map_without_ring = {}
	local pos_idx = vector.to_string(pos)

	local broken_ring = ring_map[pos_idx]

	ring_map[pos_idx] = nil

	-- find wires in broken ring
	for idx, ring in pairs(ring_map) do
		if ring == broken_ring then
			wires_to_fix[idx] = true
		end
	end

	-- remove broken ring
	ring_info[broken_ring] = nil

	for idx, _ in pairs(wires_to_fix) do
		ring_map[idx] = nil
	end

	-- readd wires from broken ring
	for idx, _ in pairs(wires_to_fix) do
		local wire_pos = vector.from_string(idx)
		local node = minetest.get_node_or_nil(wire_pos)
		if node then
			add_wire(wire_pos, node)
		end
	end

	-- fix devices
	local connected_devices = get_sides(pos, "elektron_device")
	if not connected_devices then return end
	for _, dev_pos in pairs(connected_devices) do
		add_device(dev_pos)
	end
	return
end

elektron.add_bridge = function(source, target)
	local source_idx = source:to_string()
	local target_idx = target:to_string()

	if not bridge_next[source_idx] then
		bridge_next[source_idx] = {}
	end

	bridge_next[source_idx][target_idx] = true
	
end

elektron.register_wire = function(name, def) 

	local old_after_destruct = def.after_destruct
	local old_after_place_node = def.after_place_node
	
	local default_wire = {
		drawtype = 'nodebox',
		paramtype = 'light',
		groups = { elektron_wire=1 },
		node_box = {
				type = 'connected',
				fixed = {-0.1, -0.1, -0.1, 0.1, 0.1, 0.1},
				connect_top = {-0.1, 0.1, -0.1, 0.1, 0.5, 0.1},
				connect_bottom = {-0.1, -0.1, -0.1, 0.1, -0.5, 0.1},
				connect_front = {-0.1, -0.1, -0.1, 0.1, 0.1, -0.5},
				connect_left = {-0.1, -0.1, -0.1, -0.5, 0.1, 0.1},
				connect_back = {-0.1, -0.1, 0.1, 0.1, 0.1, 0.5},
				connect_right = {0.1, -0.1, -0.1, 0.5, 0.1, 0.1},
		},

		connects_to = {name, 'group:elektron_device'},

		after_destruct=function (pos, oldnode)
			remove_wire(pos, oldnode.name)
			if old_after_destruct then old_after_destruct(pos, oldnode) end
		end,

		after_place_node=function(pos, placer, itemstack, pointed_thing)
			add_wire(pos, { name = itemstack:get_name() })
			if old_after_place_node then old_after_place_node(pos, oldnode) end
		end,
	}

	if def.groups and default_wire.groups then
		for k, v in pairs(default_wire.groups) do def.groups[k] = v end
	end
	if def.connects_to and default_wire.connects_to then
		for k, v in pairs(default_wire.connects_to) do def.connects_to[k] = v end
	end

	local def_wire = table.copy(default_wire)
	for k, v in pairs(def) do def_wire[k] = v end

	minetest.register_node(name, def_wire)
end


local remove_device = function(pos)
	local pos_idx = vector.to_string(pos)
	devices[pos_idx] = nil
end

elektron.register_device = function(name, def)

	local old_after_destruct = def.after_destruct
	local old_after_place_node = def.after_place_node

	local default_device = {

		groups = { elektron_device=1 },

		after_place_node=function(pos, placer, itemstack, pointed_thing)
			add_device(pos)
			if old_after_place_node then old_after_place_node(pos, placer, itemstack, pointed_thing) end
		end,

		after_destruct=function (pos, oldnode)
			remove_device(pos)
			if old_after_destruct then old_after_destruct(pos, oldnode) end 
		end,
	}

	if def.groups and default_device.groups then
		for k, v in pairs(default_device.groups) do def.groups[k] = v end
	end

	local def_device = table.copy(default_device)
	for k, v in pairs(def) do def_device[k] = v end

	minetest.register_node(name, def_device)

end

local tick_1 = function()
	-- POLL DEVICES
	for pos_idx, _ in pairs(devices) do
		local pos = vector.from_string(pos_idx)
		local node = minetest.get_node_or_nil(pos)	
		if node then
			local def = minetest.registered_nodes[node.name]
			if def.elektron_tick then
				def.elektron_tick(pos, devices[pos_idx])
			end
		else
			devices[pos_idx] = nil
		end
	end
end

local tick_2 = function()
	-- UPDATE BRIDGES 
	for source_idx, link in pairs(bridge) do
		for target_idx, _ in pairs(link) do
			if not (bridge_next[source_idx] and bridge_next[source_idx][target_idx]) then
				local target_ring = ring_map[target_idx]
				if target_ring then
					ring_info[target_ring] = table.copy(DEFAULT_RING_INFO)
				end
			end
		end
	end

	for source_idx, link in pairs(bridge_next) do
		for target_idx, _ in pairs(link) do
			local source_ring = ring_map[source_idx]
			local target_ring = ring_map[target_idx]
			if source_ring and target_ring then
				ring_info[target_ring] = ring_info[source_ring]
			end
		end
	end

	bridge = bridge_next
	bridge_next = {}

	
	-- UPDATE RINGs
	for key, ring in pairs(ring_info) do
		ring.power = math.min(ring.power, ring.capacity)
		ring.signal = ring.next_signal
		ring.next_signal = nil
	end

end

local add_conductor = function(pos, node)
	local def = minetest.registered_nodes[node.name]
	if def.add_conductor then 
		def.add_conductor(pos, node)
	else
		default_add_conductor(pos)	
	end
end

local cnt = 0
local tick_step = 5

minetest.register_globalstep(function(dtime)
	cnt = cnt + 1
	if cnt % (tick_step*2) == 0 then
		tick_1()
		cnt = 0
	elseif cnt % (tick_step*2) == tick_step then
		tick_2()
	end
	return
end)

-- register lbms for loading devices and wires
minetest.register_lbm({
	name = "elektron:device_lbm",
	nodenames = {"group:elektron_device"},
	run_at_every_load = true,
	action = add_device,
})

minetest.register_lbm({
	name = "elektron:wire_lbm",
	nodenames = {"group:elektron_wire"},
	run_at_every_load = true,
	action = add_wire,
})

-- Create wire nodes
elektron.register_wire('elektron:wire_red', {
	description = 'Elektron Wire Red',
	tiles = {'elektron_wire_red.png'},
	inventory_image = "elektron_wire_red_inv.png",
	groups= {oddly_breakable_by_hand =3 },
})

elektron.register_wire('elektron:wire_blue', {
	description = 'Elektron Wire Blue',
	tiles = {'elektron_wire_blue.png'},
	inventory_image = "elektron_wire_blue_inv.png",
	groups= {oddly_breakable_by_hand =3 },
})

elektron.register_wire('elektron:wire_green', {
	description = 'Elektron Wire Green',
	tiles = {'elektron_wire_green.png'},
	inventory_image = "elektron_wire_green_inv.png",
	groups= {oddly_breakable_by_hand =3 },
})

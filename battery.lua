local add_power = elektron.add_power
local register_device = elektron.register_device
local find_wire = elektron.find_wire


local THRESHOLD = 10
local def = {
	description = 'Elektron Battery',
	tiles = { 'elektron_battery.png' },
	paramtype = 'light',
	groups = {oddly_breakable_by_hand = 3},

	elektron_connect = function(pos, device)
		local _, output = next(device.wires)
		elektron.add_capacity(output, 100)
	end,
}

register_device("elektron:battery", def)

local add_power = elektron.add_power
local register_device = elektron.register_device
local find_wire = elektron.find_wire

local def = {
	description = 'Elektron lamp',
	tiles = {'elektron_lamp.png'},
	groups = {oddly_breakable_by_hand = 3},
	paramtype = 'light',

	elektron_tick = function(pos, device)
		local _, in_power = next(device.wires) -- just pick first connected wire
		local power = add_power(in_power, -1)
		if power then
			minetest.swap_node(pos, {name="elektron:lamp_on"})	
		end
		return
	end,
}
register_device("elektron:lamp",def)

-- different node for when switched on
local def_on = {
	light_source = 10,
	tiles = {'elektron_lamp_on.png'},
	groups = {not_in_creative_inventory=1, oddly_breakable_by_hand=3},
	paramtype = 'light',

	elektron_tick = function(pos, device)
		local _, in_power = next(device.wires) -- just pick first connected wire
		local power = add_power(in_power, -1)
		if not power then
			minetest.swap_node(pos, {name="elektron:lamp"})	
		end
		return
	end,

	drop = "elektron:lamp",
}
register_device("elektron:lamp_on", def_on) 

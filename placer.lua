
local add_power = elektron.add_power
local rotate_align = elektron.geometry.rotate_align
local formspec = [[
	formspec_version[6]
	size[15,11]
	no_prepend[]
	bgcolor[;true;#0000002F]
	background9[4.25,0.5  ;3.25,3.25 ;elektron_inv_bg.png;false;10]
	background9[4.5, 0.75 ;2.75,1.5  ;elektron_inv_bg2.png;false;10]
	background9[0.75,4    ;10.25,5.25;elektron_inv_bg.png;false;10]
	background9[0.85,4.1  ;10.05,1.3 ;elektron_inv_bg3.png;false;10]
	image[      5.75,   1    ;1, 1   ;elektron_placer_arrow.png]
	list[nodemeta:{POS};top;  4.75, 1    ;1,1;]
	list[nodemeta:{POS};main; 4.75, 2.5  ;2,1;]
	list[current_player;main; 1,    4.25 ;8,1;]
	list[current_player;main; 1,    5.5  ;8,3;8]
	listring[current_player;main]
	listring[nodemeta:{POS};top]
	listring[nodemeta:{POS};main]
]]

local function dig(pos, p_name)
	local node = minetest.get_node(pos)
	if minetest.is_protected(pos, p_name) then return false end
	local nodedef = minetest.registered_nodes[node.name]
	if not nodedef then return false end	

	local cracky = minetest.get_item_group(node.name, "cracky")
	local crumbly = minetest.get_item_group(node.name, "crumbly")
	local snappy = minetest.get_item_group(node.name, "snappy")
	local by_hand = minetest.get_item_group(node.name, "oddly_breakable_by_hand")

	-- if not in one of these groups - we don't dig
	if cracky > 0 or crumbly > 0 or snappy > 0 or by_hand > 0 then
	else
		return false
	end
	if nodedef.can_dig and nodedef.can_dig(pos, p_name) == false then
		return false
	end

	local drops = minetest.get_node_drops(node)
	local result = minetest.set_node(pos, {name = "air"})
	if not result then return false end

	return drops
end

local put = function(pos, p_name, stack)


	local robot = elektron.create_fake_player(
		{name = p_name, position = pos, sneak=true}
	)

	local pointed_thing = {
		type="node", 
		under=vector.add(pos,vector.new(0, -1, 0)), 
		above=pos,
	}
	local nodedef = minetest.registered_nodes[stack:get_name()]
	local place_node = nodedef.on_place or minetest.item_place_node

	local stack, placed_at = place_node(stack, robot, pointed_thing)

	return stack

end

local def = {
	description = 'Elektron Placer',
	tiles = {
		'elektron_placer.png', 'elektron_gate_power.png', -- top, bottom
		'elektron_gate_power.png','elektron_gate_power.png', -- right, left
		'elektron_gate_power.png','elektron_gate_power.png', -- back, front
	},
	groups = {oddly_breakable_by_hand = 3},
	paramtype = 'light',
	paramtype2 = 'facedir',

	after_place_node = function(pos, placer) 
		local meta = minetest.get_meta(pos)
		local owner = placer:get_player_name() or ""
		meta:set_string("owner", owner)
		local inv = meta:get_inventory()
		inv:set_size("main",2)
		inv:set_size("top",1)
	end,

	on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
		local p_name = clicker:get_player_name()
		if minetest.is_protected(pos, p_name) then return end
		local pos_str = ""..pos.x..","..pos.y..","..pos.z
		local fspec = formspec:gsub("{POS}",pos_str)
		minetest.show_formspec(p_name, "zr_elektron:placer", fspec)
	end,

	elektron_tick = function(pos, device)
		local _, in_power = next(device.wires) -- just pick first connected wire
		local meta = minetest.get_meta(pos)
		local power = add_power(in_power, -1)

		if power and not device.is_on then
			local node = minetest.get_node(pos)
			local target_offset = rotate_align(vector.new(0, 1, 0), node.param2)
			local target = pos + target_offset
			local target_node = minetest.get_node(target)
			local owner = meta:get_string("owner")
			local inv = meta:get_inventory()

			local drops = dig(target, owner)
			if drops then 
				for _, itemstring in ipairs(drops) do
					local todrop = inv:add_item("main",ItemStack(itemstring))
					--TODO: add 'drop' for overflow?
				end
			end

			local put_stack = inv:get_stack("top",1)

			if put_stack then
				if put_stack:is_empty() then
					-- move stack from main into top if top is empty
					local move_stack = inv:get_stack("main",1)
					if move_stack:is_empty() then
						move_stack = inv:get_stack("main",2)
					end
					if not move_stack:is_empty() then
						move_stack = inv:remove_item("main", move_stack)
						if not move_stack:is_empty() then
							inv:add_item("top", move_stack)
						end
					end
				else
					-- place item
					local new_stack = put(target, owner, put_stack)
					inv:set_stack("top", 1, new_stack)	
				end
			end
		end

		if power then
			device.is_on = true
		else
			device.is_on = nil
		end
	end,
}

elektron.register_device("elektron:placer", def)
